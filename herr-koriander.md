# Herr Koriander aus dem Land der Füllmengen

Eigentlich war es ein ganz normaler Tag für Herrn Koriander.
Nach der Arbeit machte er noch einen kurzen Umweg zum Drogeriemarkt, um einen neuen Seifenspender zu erwerben.
Mit dem Indiz seiner erfolgreichen Tat in der Aktentasche schlurfte er die knarrenden Treppenstufen zu seiner Altbauwohnung hoch.
Dabei umwob ihn die Motivation, die viele Menschen nach einem etwas zu sonnigen Arbeitstag ergreift, wenn es ihrem Heim zwar nicht an Aussicht aber an Fahrstuhl mangelt.
Mit einem multidimensionalem Uff-Geräusch schloss Herr Koriander die Wohnungstür auf.
Sein in den Flur gerufenes "Ich bin zu Hause" wurde nur von Spielekonsolenklängen aus dem Wohnzimmer beantwortet.
Verächtlich hmpfend betrat Herr Koriander das Badezimmer.
Er stellte leicht triumphierend seine mit Flüssigseife prall gefüllte Errungenschaft neben die seit dem Vortag kläglich erscheinende, leere Hülle vergangener Seifenspendeerlebnisse.
Den aufmerksam Lesenden sei hier noch versichert, dass sich Herr Koriander selbstverständlich vor dem Betreten des Badezimmers die Straßenschuhe auszog.
Plötzlich wurde ihm schlagartig bewusst, dass etwas nicht stimmte.
Ein Vergleich zurückgebliebener Flüssigseifenreste im ausgedienten Haushaltsutensil mit dem Pegel im neusten Addendum der Wohneinheit deutete auf Undenkbares hin.
Herr Koriander wich überrascht zurück und rief einer spontanen Eingebung folgend: "Peter?"
Hiermit war nicht etwa einer der Seifenspender oder gar ein Einrichtungsgegenstand des Badezimmers gemeint.
Vielmehr handelte es sich um den Vornamen des Ehemanns von Herrn Koriander, den dieser Spielekonsole spielend im Wohnzimmer vermutete.
Als eine Antwort ausblieb, wurde die Frage noch um ein leicht aufgebrachtes "Herr Peter Silie!" ergänzt, womit der Autor subtil den Nachnamen von Herrn Korianders besserer Hälfte verkündete.
Handlungseifer durchströmte Hände, die nun nach einem mit Flüssigseife gefüllten Seifenspender griffen.
Suchende Augen fanden schließlich die Angabe der Füllmenge.
Sogar ein ungläubiges Blinzeln konnte nicht helfen.
Wo Herr Koriander seit Jahren 503ml las, stand nun nur noch 500ml.
All dies geschah ohne die Öffentlichkeit zu informieren.
Nicht einmal ein "Verbesserte Rezeptur" prangte auf der Verpackung.
Herr Koriander fühlte sich um fast 0,6% seiner Seife betrogen.
